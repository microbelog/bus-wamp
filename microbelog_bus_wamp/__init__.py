import asyncio
from autobahn.asyncio import wamp, websocket
import logging

logger = logging.getLogger(__name__)

DEFAULT_TOPIC = 'org.microbelog'

class WampBus(wamp.ApplicationSession):
    def __init__(self, topic=DEFAULT_TOPIC):
        self._incoming = asyncio.Queue()
        self.topic = topic

    def onConnect(self):
        self.join('realm1')

    @asyncio.coroutine
    def onJoin(self, details):
        sub = yield from self.subscribe(self.onEvent, self.topic)
        logger.debug('Subscribed to %s with id %s', self.topic, sub.id)
    
    def onEvent(self, event):
        asyncio.async(self._incoming.put(event))

    @asyncio.coroutine
    def send(self, event):
        self.publish(self.topic, event)
        logger.debug('Put message: "%s" to topic %s', event, self.topic)
        print("putting message: {}".format(msg))
        yield from self._queue.put(msg)

    @asyncio.coroutine
    def recv(self):
        event = yield from self._incoming.get()
        #print("received message: {}".format(msg))
        return event

def create_bus():
    return WampBus()

def main():
    bus = WampBus()

    def create_bus():
        return bus

    session_factory = wamp.ApplicationSessionFactory()
    session_factory.session = create_bus
    transport_factory = websocket.WampWebSocketClientFactory(session_factory,
                                                             debug = False,
                                                             debug_wamp = False)

    loop = asyncio.get_event_loop()

    coro = loop.create_connection(transport_factory, '127.0.0.1', 8080)
    server = loop.run_until_complete(coro)

    roundtrip = asyncio.Future()

    @asyncio.coroutine
    def do_recv(bus, future):
        message = yield from bus.recv()
        future.set_result(message)

    asyncio.async(bus.send('[body: "Luke, I am your father."]'))
    asyncio.async(do_recv(bus, roundtrip))

    try:
        loop.run_until_complete(roundtrip)
    except KeyboardInterrupt:
        pass
    finally:
        server.close()
        loop.close()

    print('result: {}'.format(future.result()))

if __name__ == '__main__':
    main()
