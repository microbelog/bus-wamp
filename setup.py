from setuptools import setup, find_packages
import sys, os

version = '0.1.0'

requires = [
    "autobahn",
]

setup(name='microbelog-bus-wamp',
      version=version,
      description="A WAMP adapter for the MicrobeLog pluggable bus",
      long_description="""\
Puts messages to the configured WAMP broker+topic, gets messages from the configured WAMP broker+topic.""",
      classifiers=[], # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
      keywords='messaging bus',
      author='Claes Wallin (\xe9\x9f\x8b\xe5\x98\x89\xe8\xaa\xa0)',
      author_email='claes.wallin@greatsinodevelopment.com',
      url='https://gitlab.com/groups/microbelog',
      license='copyleft-next 0.3.0',
      packages=find_packages(exclude=['ez_setup', 'examples', 'tests']),
      include_package_data=True,
      zip_safe=False,
      install_requires=requires,
      entry_points="""
      # -*- Entry points: -*-
      """,
      )
